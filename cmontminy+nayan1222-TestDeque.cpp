// -------------
// TestDeque.c++
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range
#include <vector>

#include "gtest/gtest.h"

#include "Deque.hpp"

// -----
// Using
// -----

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;};

using
    deque_types =
    Types<
           deque<int>,
        my_deque<int>,
           deque<int, allocator<int>>,
        my_deque<int, allocator<int>>>;

#ifdef __APPLE__
    TYPED_TEST_CASE(DequeFixture, deque_types,);
#else
    TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

TYPED_TEST(DequeFixture, test1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(0);
    ASSERT_TRUE(x.empty());
    EXPECT_TRUE(x.size() == 0);
    x.push_back(1);
    x.push_front(2);
    x.push_back(3);
    auto b = begin(x);
    auto e = end(x);
    EXPECT_TRUE(*b == 2);
    ++b;
    EXPECT_TRUE(*b == 1);
    ++b;
    EXPECT_TRUE(*b == 3);
    ++b;
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test2) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(1);
    iterator b = begin(x);
    iterator e = end(x);
    EXPECT_TRUE(b != e);}

TYPED_TEST(DequeFixture, test3) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x(0);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test4){
    using deque_type = typename TestFixture::deque_type;

    deque_type x(10, 2);
    deque_type y(10, 3);
    EXPECT_TRUE(x != y);
}

TYPED_TEST(DequeFixture, test5){
    using deque_type = typename TestFixture::deque_type;

    deque_type x(10, 2);
    deque_type y(10, 3);
    EXPECT_TRUE(x < y);
}

TYPED_TEST(DequeFixture, test6){
    using deque_type = typename TestFixture::deque_type;

    deque_type x(10, 3);
    deque_type y(10, 3);
    EXPECT_TRUE(x == y);
}

TYPED_TEST(DequeFixture, test7){
    using deque_type = typename TestFixture::deque_type;

    deque_type x(10, 2);
    auto b1 = begin(x);
    auto b2 = end(x);
    EXPECT_TRUE(b1 != b2);
}

//iterator +=
TYPED_TEST(DequeFixture, test8){
    using deque_type = typename TestFixture::deque_type;

    deque_type x(2);
    auto b1 = begin(x);
    auto b2 = end(x);
    b1 += 2;
    EXPECT_TRUE(b1 == b2);
}

TYPED_TEST(DequeFixture, test9){
    using deque_type = typename TestFixture::deque_type;

    deque_type x(2);
    auto b = begin(x);
    auto e = end(x);
    e -= 2;
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test10){
    using deque_type = typename TestFixture::deque_type;

    deque_type x(2);
    auto b1 = begin(x);
    auto b2 = end(x);
    ++b1;
    --b2;
    EXPECT_TRUE(b1 == b2);
}

TYPED_TEST(DequeFixture, test11){
    using deque_type = typename TestFixture::deque_type;

    deque_type x(1, 5);
    auto b1 = begin(x);
    EXPECT_TRUE(*b1 == 5);
}

TYPED_TEST(DequeFixture, test12){
    using deque_type = typename TestFixture::deque_type;

    deque_type x(1, 5);
    auto b = begin(x);
    auto e = end(x);
    b++;
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test13){
    using deque_type = typename TestFixture::deque_type;

    deque_type x(1, 5);
    auto b = begin(x);
    auto e = end(x);
    e--;
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test14){
    using deque_type = typename TestFixture::deque_type;

    deque_type x(3, 5);
    auto b = begin(x);
    auto e = end(x);
    b += 3;
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test15){
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;

    const deque_type x(10, 2);
    const_iterator b1 = begin(x);
    const_iterator b2 = end(x);
    EXPECT_TRUE(b1 != b2);
}

TYPED_TEST(DequeFixture, test16){
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;

    const deque_type x(2);
    const_iterator b1 = begin(x);
    const_iterator b2 = end(x);
    b1 += 2;
    EXPECT_TRUE(b1 == b2);
}

TYPED_TEST(DequeFixture, test17){
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;

    const deque_type x(2);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    e -= 2;
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test18){
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;

    const deque_type x(2);
    const_iterator b1 = begin(x);
    const_iterator b2 = end(x);
    ++b1;
    EXPECT_TRUE(b1 != b2);
}

TYPED_TEST(DequeFixture, test19){
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;

    const deque_type x(1, 5);
    const_iterator b1 = begin(x);
    EXPECT_TRUE(*b1 == 5);
}

TYPED_TEST(DequeFixture, test20){
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;

    const deque_type x(1, 5);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    b++;
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test21){
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;

    const deque_type x(1, 5);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    e--;
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test22){
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;

    const deque_type x(25, 5);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    b += 12;
    e -= 13;
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test23){
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;

    const deque_type x(2, 5);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    b += 2;
    EXPECT_TRUE(b == e);
}

// //const_iterator -=
TYPED_TEST(DequeFixture, test24){
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;

    const deque_type x(2, 5);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    e -= 2;
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test25){
    using deque_type = typename TestFixture::deque_type;

    deque_type x(10);
    EXPECT_TRUE(x.size() == 10);
}

TYPED_TEST(DequeFixture, test26){
    using deque_type = typename TestFixture::deque_type;

    deque_type x({2, 3, 4});
    deque_type y({4, 3, 2});
    EXPECT_TRUE(x != y);
    auto b = begin(x);
    auto e = end(y);
    --e;
    EXPECT_TRUE(*b == *e);
}

TYPED_TEST(DequeFixture, test27){
    using deque_type = typename TestFixture::deque_type;
    using allocator_type = typename TestFixture::allocator_type;

    deque_type x(10, 5, allocator_type());
    EXPECT_TRUE(x.size() == 10);
}

TYPED_TEST(DequeFixture, test28){
    using deque_type = typename TestFixture::deque_type;
    using allocator_type = typename TestFixture::allocator_type;

    deque_type x({1, 2, 3}, allocator_type());
    auto b = begin(x);
    EXPECT_TRUE(*b == 1);
    ++b;
    EXPECT_TRUE(*b == 2);
    ++b;
    EXPECT_TRUE(*b == 3);
    EXPECT_TRUE(x.size() == 3);
}

TYPED_TEST(DequeFixture, test29){
    using deque_type = typename TestFixture::deque_type;
    using allocator_type = typename TestFixture::allocator_type;

    deque_type x(10, 5, allocator_type());
    deque_type y(x);
    EXPECT_TRUE(x == y);
}

TYPED_TEST(DequeFixture, test30){
    using deque_type = typename TestFixture::deque_type;
    using allocator_type = typename TestFixture::allocator_type;

    deque_type x(10, 5, allocator_type());
    deque_type y({1, 2, 3});
    x = y;
    EXPECT_TRUE(x.size() == 3);
}

TYPED_TEST(DequeFixture, test31){
    using deque_type = typename TestFixture::deque_type;

    deque_type y({1, 2, 3});
    auto b = begin(y);
    ++b;
    EXPECT_TRUE(y[1] == 2);
    deque_type x(200, 10);
    for(int i = 1; i <= 5; ++i)
        x.push_back(i);
    EXPECT_TRUE(x[x.size() - 1] == 5);
    EXPECT_TRUE(x[x.size() - 2] == 4);
    EXPECT_TRUE(x[x.size() - 3] == 3);
    EXPECT_TRUE(x[x.size() - 4] == 2);
    EXPECT_TRUE(x[x.size() - 5] == 1);

    for(int i = 0; i < 5; ++i)
        x.push_front(i);
    EXPECT_TRUE(x[0] == 4);
    EXPECT_TRUE(x[1] == 3);
    EXPECT_TRUE(x[2] == 2);
    EXPECT_TRUE(x[3] == 1);
    EXPECT_TRUE(x[4] == 0);
    EXPECT_TRUE(x[5] == 10);
}

TYPED_TEST(DequeFixture, test32){
    using deque_type = typename TestFixture::deque_type;

    deque_type y({1, 2, 3});
    EXPECT_TRUE(y.at(2) == 3);
    EXPECT_THROW(y.at(5), std::out_of_range);
    EXPECT_TRUE(y.at(0) == 1);
}

TYPED_TEST(DequeFixture, test33){
    using deque_type = typename TestFixture::deque_type;

    deque_type y({1, 2, 3, 5, 4});
    EXPECT_TRUE(y.back() == 4);
    y.resize(1);
    EXPECT_TRUE(y.back() == y.front());
}

TYPED_TEST(DequeFixture, test34){
    using deque_type = typename TestFixture::deque_type;

    deque_type y({1, 2, 3, 5, 4});
    EXPECT_TRUE(y.front() == 1);
    y.resize(50, 7);
    EXPECT_TRUE(y.size() == 50);
    EXPECT_TRUE(y.at(5) == 7);
    EXPECT_TRUE(y.at(0) == 1);
    EXPECT_TRUE(y.at(1) == 2);
    EXPECT_TRUE(y.at(2) == 3);
    EXPECT_TRUE(y.at(3) == 5);
    EXPECT_TRUE(y.at(4) == 4);
    EXPECT_TRUE(y.at(y.size() - 1) == 7);
}

TYPED_TEST(DequeFixture, test35){
    using deque_type = typename TestFixture::deque_type;

    deque_type y({1, 2, 3, 5, 4});
    y.clear();
    EXPECT_TRUE(y.size() == 0);
}

TYPED_TEST(DequeFixture, test36){
    using deque_type = typename TestFixture::deque_type;

    deque_type y({1, 2, 3, 5, 4});
    y.clear();
    EXPECT_TRUE(y.empty());
    y.push_back(1);
    y.push_front(2);
    deque_type answer({2, 1});
    EXPECT_TRUE(y == answer);
}

TYPED_TEST(DequeFixture, test37){
    using deque_type = typename TestFixture::deque_type;

    deque_type y({1, 2, 3, 5, 4});
    auto b = begin(y);
    b += 2;
    y.erase(b);
    deque_type z({1, 2, 5, 4});
    EXPECT_TRUE(y == z);
}

TYPED_TEST(DequeFixture, test38){
    using deque_type = typename TestFixture::deque_type;

    deque_type y({1, 2, 3, 5, 4});
    auto b = begin(y);
    b += 2;
    int num = 6;
    y.insert(b, num);
    deque_type z({1, 2, 6, 3, 5, 4});
    EXPECT_TRUE(y == z);
}

TYPED_TEST(DequeFixture, test39){
    using deque_type = typename TestFixture::deque_type;

    deque_type y({1, 2, 3, 5, 4});
    y.pop_back();
    deque_type z({1, 2, 3, 5});
    EXPECT_TRUE(y == z);
}

TYPED_TEST(DequeFixture, test40){
    using deque_type = typename TestFixture::deque_type;

    deque_type y({1, 2, 3, 5, 4});
    y.pop_front();
    deque_type z({2, 3, 5, 4});
    EXPECT_TRUE(y == z);
}

TYPED_TEST(DequeFixture, test41){
    using deque_type = typename TestFixture::deque_type;

    deque_type y({1, 2, 3, 5, 4});
    int num = 6;
    y.push_back(num);
    deque_type z({1, 2, 3, 5, 4, 6});
    EXPECT_TRUE(y == z);
}

TYPED_TEST(DequeFixture, test42){
    using deque_type = typename TestFixture::deque_type;

    deque_type y({1, 2, 3, 5, 4});
    int num = 6;
    y.push_front(num);
    deque_type z({6, 1, 2, 3, 5, 4});
    EXPECT_TRUE(y == z);
}

TYPED_TEST(DequeFixture, test43){
    using deque_type = typename TestFixture::deque_type;

    deque_type y({1, 2, 3, 5, 4});
    y.resize(2);
    EXPECT_TRUE(y.size() == 2);
    deque_type answer({1, 2});
    EXPECT_TRUE(y == answer);
}

TYPED_TEST(DequeFixture, test44){
    using deque_type = typename TestFixture::deque_type;

    deque_type y({1, 2, 3, 5, 4});
    int num = 6;
    y.push_back(num);
    deque_type z({1, 2, 3, 5, 4, 6});
    EXPECT_TRUE(y == z);
}

TYPED_TEST(DequeFixture, test45){
    using deque_type = typename TestFixture::deque_type;

    deque_type y({1, 2, 3});
    int num = 5;
    y.resize(5, num);
    deque_type z({1, 2, 3, 5, 5});
    EXPECT_TRUE(y == z);
}

TYPED_TEST(DequeFixture, test46){
    using deque_type = typename TestFixture::deque_type;

    deque_type y({1, 2, 3});
    deque_type x({4, 5, 6, 7});
    y.swap(x);
    deque_type w({4, 5, 6, 7});
    deque_type z({1, 2, 3});
    EXPECT_TRUE(y == w);
    EXPECT_TRUE(x == z);
    EXPECT_TRUE(y.size() == 4);
    EXPECT_TRUE(x.size() == 3);
}

TYPED_TEST(DequeFixture, test47){
    using deque_type = typename TestFixture::deque_type;

    deque_type y(0);
    y.resize(5);
    deque_type z({0, 0, 0, 0, 0});
    EXPECT_TRUE(y == z);
}