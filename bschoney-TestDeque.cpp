// -------------
// TestDeque.c++
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// -----
// Using
// -----

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;};

using
    deque_types =
    Types<
        // deque<int>,
        // deque<int, allocator<int>>,
        my_deque<int>,
        my_deque<int, allocator<int>>
    >;

#ifdef __APPLE__
    TYPED_TEST_CASE(DequeFixture, deque_types,);
#else
    TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

TYPED_TEST(DequeFixture, test1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);}

TYPED_TEST(DequeFixture, test2) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    EXPECT_TRUE(b == e);}

TYPED_TEST(DequeFixture, test3) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x {};
    const_iterator b = begin(x);
    const_iterator e = end(x);
    EXPECT_TRUE(b == e);}

// --------
//  const < 
// --------

TYPED_TEST(DequeFixture, clt0) {
    using deque_type     = typename TestFixture::deque_type;
    const deque_type x {1, 2, 3, 4};
    const deque_type y {1, 2, 5, 4};
    EXPECT_TRUE(x < y);}

TYPED_TEST(DequeFixture, clt1) {
    using deque_type     = typename TestFixture::deque_type;
    const deque_type x {1, 0};
    const deque_type y {2};
    EXPECT_TRUE(x < y);}

TYPED_TEST(DequeFixture, clt2) {
    using deque_type     = typename TestFixture::deque_type;
    const deque_type x {1};
    const deque_type y {2, 0};
    EXPECT_TRUE(x < y);}

TYPED_TEST(DequeFixture, clt3) {
    using deque_type     = typename TestFixture::deque_type;
    const deque_type x {1, 2, 3, 4};
    const deque_type y {1, 2, 2, 5};
    EXPECT_FALSE(x < y);}

TYPED_TEST(DequeFixture, clt4) {
    using deque_type     = typename TestFixture::deque_type;
    const deque_type x {1, 2, 3, 4};
    const deque_type y {1, 2, 3, 4};
    EXPECT_FALSE(x < y);}

// --
//  < 
// --

TYPED_TEST(DequeFixture, lt0) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {1, 2, 3, 4};
    deque_type y {1, 2, 5, 4};
    EXPECT_TRUE(x < y);}

TYPED_TEST(DequeFixture, lt1) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {1, 0};
    deque_type y {2};
    EXPECT_TRUE(x < y);}

TYPED_TEST(DequeFixture, lt2) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {1};
    deque_type y {2, 0};
    EXPECT_TRUE(x < y);}

TYPED_TEST(DequeFixture, lt3) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {1, 2, 3, 4};
    deque_type y {1, 2, 2, 5};
    EXPECT_FALSE(x < y);}

TYPED_TEST(DequeFixture, lt4) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {1, 2, 3, 4};
    deque_type y {1, 2, 3, 4};
    EXPECT_FALSE(x < y);}

// ---
//  == 
// ---

TYPED_TEST(DequeFixture, eq0) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {1, 2, 3, 4};
    deque_type y {1, 2, 3, 4};
    EXPECT_TRUE(x == y);}

TYPED_TEST(DequeFixture, eq1) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {};
    deque_type y {};
    EXPECT_TRUE(x == y);}

TYPED_TEST(DequeFixture, eq2) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {1};
    deque_type y {};
    EXPECT_FALSE(x == y);}


// ------------
//  iterator == 
// ------------

TYPED_TEST(DequeFixture, it_eq0) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {};
    EXPECT_TRUE(begin(x) == end(x));}

TYPED_TEST(DequeFixture, it_eq1) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {1, 2, 3, 4};
    deque_type y {1, 2, 3, 4};
    EXPECT_FALSE(begin(x) == begin(y));}

TYPED_TEST(DequeFixture, it_eq2) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {1, 2, 3, 4};
    deque_type y {1, 2, 3, 4};
    auto begin_x = begin(x);
    auto begin_y = begin(y);
    ++begin_y;
    EXPECT_FALSE(begin_x == begin_y);}

TYPED_TEST(DequeFixture, it_eq3) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {1, 2, 3, 4};
    EXPECT_TRUE(begin(x) == begin(x));}

// ------------
//  iterator 
// ------------

TYPED_TEST(DequeFixture, it0) {
    using deque_type     = typename TestFixture::deque_type;
    const deque_type x {1, 2, 3, 4};
    const deque_type y {1, 2, 3, 4};
    auto begin_x = begin(x);
    auto begin_y = begin(y);
    ASSERT_EQ(*begin_x, *begin_y);
    ++begin_x;
    ++begin_y;
    ASSERT_EQ(*begin_x, *begin_y);
}

TYPED_TEST(DequeFixture, it1) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {1, 2, 3, 4};

    auto begin_x = begin(x);
    auto end_x = end(x);

    x.push_front(0);
    ASSERT_EQ(*begin_x, 1);

    ++begin_x;
    x.push_front(-1);
    ASSERT_EQ(*begin_x, 2);

    x.push_front(-2);
    ++begin_x;
    ASSERT_EQ(*begin_x, 3);

    ++begin_x;
    ++begin_x;
    x.push_back(5);
    EXPECT_TRUE(begin_x == end_x);
}

TYPED_TEST(DequeFixture, it2) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {1, 2, 3, 4};

    auto begin_x = begin(x);
    x.push_front(0);
    ASSERT_EQ(*begin_x, 1);

    --begin_x;
    ASSERT_EQ(*begin_x, 0);
    
    x.push_front(-1);
    --begin_x;
    ASSERT_EQ(*begin_x, -1);
}

TYPED_TEST(DequeFixture, it3) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {1, 2, 3, 4};

    auto begin_x = begin(x);
    x.push_front(0);
    --begin_x;
    ASSERT_EQ(*begin_x, 0);

    x.pop_front();
    x.pop_front();
    x.pop_front();
    ASSERT_EQ(*begin_x, 0);
}

TYPED_TEST(DequeFixture, it4) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {-1, -2, -3, -4};

    auto begin_x = begin(x);
    auto end_x = end(x);

    for (int i = 1; i <= 10000; ++i) {
        x.push_front(i);
    }
    
    ASSERT_EQ(*begin_x, -1);
    --begin_x;
    ASSERT_EQ(*begin_x, 1);
    --end_x;
    ASSERT_EQ(*end_x, -4);
}

// -----------
//  push_front 
// -----------

TYPED_TEST(DequeFixture, pf0) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {1, 2};

    x.push_front(0);
    x.push_front(-1);
    x.push_front(-2);
    ASSERT_EQ(x.size(), 5);
    ASSERT_EQ(x[0], -2);
    ASSERT_EQ(x[4], 2);
    
    auto begin_x = begin(x);
    auto end_x = end(x);

    ASSERT_EQ(*begin_x, -2);
    ++begin_x;
    ASSERT_EQ(*begin_x, -1);
    ++begin_x;
    ASSERT_EQ(*begin_x, 0);
    ++begin_x;
    ASSERT_EQ(*begin_x, 1);
    ++begin_x;
    ASSERT_EQ(*begin_x, 2);
    ++begin_x;
    
    EXPECT_TRUE(begin_x == end_x);
}

// -----------
//  push_back 
// -----------

TYPED_TEST(DequeFixture, pb0) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {1, 2};

    x.push_back(3);
    ASSERT_EQ(x[2], 3);
    x.push_back(4);
    x.push_back(5);
    ASSERT_EQ(x.size(), 5);
    ASSERT_EQ(x[0], 1);
    ASSERT_EQ(x[4], 5);
    
    auto begin_x = begin(x);
    auto end_x = end(x);

    --end_x;
    ASSERT_EQ(*end_x, 5);
    --end_x;
    ASSERT_EQ(*end_x, 4);
    --end_x;
    ASSERT_EQ(*end_x, 3);
    --end_x;
    ASSERT_EQ(*end_x, 2);
    --end_x;
    ASSERT_EQ(*end_x, 1);
    
    EXPECT_TRUE(begin_x == end_x);
}

// -------
//  erase
// -------

TYPED_TEST(DequeFixture, erase0) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {1, 2, 3, 4, 5};

    auto b = x.begin();
    ++++b;
    ASSERT_EQ(*b, 3);

    auto res = x.erase(b);

    ASSERT_EQ(x.size(), 4);
    ASSERT_EQ(*res, 4);
    ++res;
    ASSERT_EQ(*res, 5);
}

TYPED_TEST(DequeFixture, erase1) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {1};

    auto b = x.begin();
    ASSERT_EQ(*b, 1);

    x.erase(b);

    EXPECT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0);
}

TYPED_TEST(DequeFixture, erase2) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;

    for (int i = 1; i <= 1000; ++i) {
        x.push_back(i);
    }

    for (int i = 1; i <= 1000; ++i) {
        auto b = x.begin();
        ASSERT_EQ(*b, i);
        x.erase(b);
    }

}

// -------
//  insert
// -------

TYPED_TEST(DequeFixture, insert0) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;

    auto res1 = x.insert(begin(x), 100);
    ASSERT_EQ(*res1, 100);

    auto res2 = x.insert(begin(x), -10);
    ASSERT_EQ(*res2, -10);

    ASSERT_EQ(x[0], -10);
    ASSERT_EQ(x[1], 100);
    ASSERT_EQ(x.size(), 2);
}

TYPED_TEST(DequeFixture, insert1) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(25, 44);

    ASSERT_EQ(x.size(), 25);

    auto begin_x = begin(x);
    int insertion_index = 10;
    for (int i = 0; i < insertion_index; ++i) {
        ++begin_x;
    }

    auto res1 = x.insert(begin_x, 12345);
    ASSERT_EQ(*res1, 12345);
    ASSERT_EQ(x.size(), 26);
    ASSERT_EQ(x[insertion_index], 12345);
    
    for (int j = insertion_index + 1; j < 26; ++j) {
        ASSERT_EQ(x[j], 44);
    }
}

TYPED_TEST(DequeFixture, insert2) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {1, 2, 3, 5, 6};

    auto begin_x = begin(x);
    ASSERT_EQ(*begin_x, 1);
    ++begin_x;
    ASSERT_EQ(*begin_x, 2);
    ++begin_x;
    ASSERT_EQ(*begin_x, 3);
    ++begin_x;
    x.insert(begin_x, 4);

    ASSERT_EQ(x.size(), 6);
    auto begin1_x = begin(x);
    ASSERT_EQ(*begin1_x, 1);
    ++begin1_x;
    ASSERT_EQ(*begin1_x, 2);
    ++begin1_x;
    ASSERT_EQ(*begin1_x, 3);
    ++begin1_x;
    ASSERT_EQ(*begin1_x, 4);
    ++begin1_x;
    ASSERT_EQ(*begin1_x, 5);
    ++begin1_x;
    ASSERT_EQ(*begin1_x, 6);
}

// -----------------
//  copy constructor
// -----------------

TYPED_TEST(DequeFixture, copy0) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {-2, -1, 1, 2, 3, 4, 5};
    deque_type y(x);

    auto begin_x = begin(x);
    auto begin_y = begin(y);
    ASSERT_EQ(*begin_x, -2);
    ASSERT_EQ(*begin_x, *begin_y);
    
    auto end_x = end(x);
    auto end_y = end(y);
    --end_x;
    --end_y;
    ASSERT_EQ(*end_x, 5);
    ASSERT_EQ(*end_x, *end_y);
}

TYPED_TEST(DequeFixture, copy1) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    deque_type y(x);

    EXPECT_TRUE(x == y);
}

// ------------
//  operator =
// ------------

TYPED_TEST(DequeFixture, assignment0) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {1, 2, 3, 4, 5};
    deque_type y {5, 4, 3, 2, 1};

    x = y;

    EXPECT_TRUE(x == y);
}

TYPED_TEST(DequeFixture, assignment1) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {1, 2, 3, 4, 5};
    deque_type y = x;

    EXPECT_TRUE(x == y);
}

// ------
//  clear
// ------

TYPED_TEST(DequeFixture, clear0) {
    using deque_type     = typename TestFixture::deque_type;

    deque_type x {-2, -1, 1, 2, 3, 4, 5};

    x.clear();
    EXPECT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0);

    x.push_front(15);
    ASSERT_EQ(x[0], 15);
    ASSERT_EQ(x.size(), 1);
}

TYPED_TEST(DequeFixture, clear1) {
    using deque_type     = typename TestFixture::deque_type;

    deque_type x;

    EXPECT_TRUE(x.empty());

    x.clear();
    EXPECT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0);

    x.push_front(15);
    ASSERT_EQ(x[0], 15);
    ASSERT_EQ(x.size(), 1);
}

// ---------
//  pop_back
// ---------

TYPED_TEST(DequeFixture, popback0) {
    using deque_type     = typename TestFixture::deque_type;

    deque_type x {-2, -1, 1, 2, 3, 4, 5};

    ASSERT_EQ(x[x.size() - 1], 5);
    x.pop_back();
    ASSERT_EQ(x[x.size() - 1], 4);
    x.pop_back();
    ASSERT_EQ(x[x.size() - 1], 3);
    x.pop_back();
    ASSERT_EQ(x[x.size() - 1], 2);
    x.pop_back();
    ASSERT_EQ(x[x.size() - 1], 1);
    x.pop_back();
    ASSERT_EQ(x[x.size() - 1], -1);
    x.pop_back();
    ASSERT_EQ(x[x.size() - 1], -2);
    x.pop_back();

    EXPECT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0);
}

// ----------
//  pop_front
// ----------

TYPED_TEST(DequeFixture, popfront0) {
    using deque_type     = typename TestFixture::deque_type;

    deque_type x {-2, -1, 1, 2, 3, 4, 5};

    ASSERT_EQ(x[0], -2);
    x.pop_front();
    ASSERT_EQ(x[0], -1);
    x.pop_front();
    ASSERT_EQ(x[0], 1);
    x.pop_front();
    ASSERT_EQ(x[0], 2);
    x.pop_front();
    ASSERT_EQ(x[0], 3);
    x.pop_front();
    ASSERT_EQ(x[0], 4);
    x.pop_front();
    ASSERT_EQ(x[0], 5);
    x.pop_front();

    EXPECT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0);
}

// ------
//  back
// ------

TYPED_TEST(DequeFixture, back0) {
    using deque_type     = typename TestFixture::deque_type;

    deque_type x {-2, -1, 1, 2, 3, 4, 5};

    ASSERT_EQ(x.back(), 5);
    x.pop_back();
    ASSERT_EQ(x.back(), 4);
    x.pop_back();
    ASSERT_EQ(x.back(), 3);
    x.pop_back();
    ASSERT_EQ(x.back(), 2);
    x.pop_back();
    ASSERT_EQ(x.back(), 1);
    x.pop_back();
    ASSERT_EQ(x.back(), -1);
    x.pop_back();
    ASSERT_EQ(x.back(), -2);
    x.pop_back();

    EXPECT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0);
}

// ------
//  front
// ------

TYPED_TEST(DequeFixture, front0) {
    using deque_type     = typename TestFixture::deque_type;

    deque_type x {-2, -1, 1, 2, 3, 4, 5};

    ASSERT_EQ(x.front(), -2);
    x.pop_front();
    ASSERT_EQ(x.front(), -1);
    x.pop_front();
    ASSERT_EQ(x.front(), 1);
    x.pop_front();
    ASSERT_EQ(x.front(), 2);
    x.pop_front();
    ASSERT_EQ(x.front(), 3);
    x.pop_front();
    ASSERT_EQ(x.front(), 4);
    x.pop_front();
    ASSERT_EQ(x.front(), 5);
    x.pop_front();

    EXPECT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0);
}

// ------
//  swap
// ------

TYPED_TEST(DequeFixture, swap0) {
    using deque_type     = typename TestFixture::deque_type;

    deque_type x {-2, -1, 1, 2, 3, 4, 5};
    deque_type y (105, 10);
    deque_type a {-2, -1, 1, 2, 3, 4, 5};
    deque_type b (105, 10);

    x.swap(y);
    EXPECT_TRUE(y == a);
    EXPECT_TRUE(x == b);

}

TYPED_TEST(DequeFixture, swap1) {
    using deque_type     = typename TestFixture::deque_type;

    deque_type x {-2, -1, 1, 2, 3, 4, 5};
    deque_type y (105, 10);
    deque_type& a = x;
    deque_type& b = y;

    x.swap(y);

    ASSERT_EQ(&y, &b);
    ASSERT_EQ(&x, &a);

}

TYPED_TEST(DequeFixture, swap2) {
    using deque_type     = typename TestFixture::deque_type;

    deque_type x {-2, -1, 1, 2, 3, 4, 5};
    deque_type& y = x;

    x.swap(y);

    ASSERT_EQ(x, y);
    ASSERT_EQ(&x, &y);

}
