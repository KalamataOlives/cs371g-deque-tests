// -------------
// TestDeque.c++
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// -----
// Using
// -----

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename F>
struct DequeFixture : Test {
    using deque_type    = F;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};

using
deque_types =
    Types<
    deque<int>,
    my_deque<int>,
    deque<int, allocator<int>>,
    my_deque<int, allocator<int>>
    >;

#ifdef __APPLE__
TYPED_TEST_CASE(DequeFixture, deque_types,);
#else
TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

TYPED_TEST(DequeFixture, test1) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);
}

TYPED_TEST(DequeFixture, test2) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    x.push_back(3);
    ASSERT_EQ(x.size(), 1);
    ASSERT_EQ(x[0], 3);
}
TYPED_TEST(DequeFixture, copy_const0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    x.push_back(3);
    deque_type y(x);
    ASSERT_EQ(y.size(), 1);
    ASSERT_EQ(y[0], 3);
    ASSERT_EQ(x,y);
}

TYPED_TEST(DequeFixture, it0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, const_it0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::const_iterator;
    const deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, pushback1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    x.push_back(3);
    ASSERT_EQ(x[0], 3);
    x.push_back(4);
    x.push_back(5);
    ASSERT_EQ(x.size(), 3);
    ASSERT_EQ(x[0], 3);
    ASSERT_EQ(x[1], 4);
    ASSERT_EQ(x[2], 5);
}

TYPED_TEST(DequeFixture, insert0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    x.push_back(3);
    x.push_back(4);
    iterator b = x.begin();
    ++b;
    x.insert(b, 5);
    ASSERT_EQ(*b, 5);
    ASSERT_EQ(x.size(), 3);
    ASSERT_EQ(x[0], 3);
    ASSERT_EQ(x[1], 5);
    ASSERT_EQ(x[2], 4);
}
TYPED_TEST(DequeFixture, push_front_0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    x.push_front(3);
    ASSERT_EQ(x.size(), 1);
    ASSERT_EQ(x[0], 3);
}

TYPED_TEST(DequeFixture, insert00) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    x.push_back(3);
    x.push_back(4);
    iterator b = x.begin();
    x.insert(b, 5);
    ASSERT_EQ(*b, 3);
    ASSERT_EQ(x.size(), 3);
    ASSERT_EQ(x[0], 5);
    ASSERT_EQ(x[1], 3);
    ASSERT_EQ(x[2], 4);
}

TYPED_TEST(DequeFixture, it1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    x.push_back(3);
    x.push_back(4);
    iterator b = x.begin();
    x.insert(b, 5);
    b = x.begin();
    ASSERT_EQ(x.size(), 3);
    ASSERT_EQ(*b, 5);
    ASSERT_EQ(*++b, 3);
    ASSERT_EQ(*++b, 4);
}
TYPED_TEST(DequeFixture, const_it1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    using const_iterator   = typename TestFixture::const_iterator;
    deque_type x;
    x.push_back(3);
    x.push_back(4);
    iterator b = x.begin();
    x.insert(b, 5);
    const deque_type y(x);
    const_iterator bb = y.begin();
    ASSERT_EQ(y, x);
    ASSERT_EQ(y.size(), 3);
    ASSERT_EQ(*bb, 5);
    ASSERT_EQ(*++bb, 3);
    ASSERT_EQ(*++bb, 4);
}

TYPED_TEST(DequeFixture, it2) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(10);
    x.push_back(3);
    x.push_back(4);
    iterator b = x.end();
    ASSERT_EQ(x.size(), 12);
    ASSERT_EQ(*--b, 4);
    ASSERT_EQ(*--b, 3);
    ASSERT_EQ(*--b, 0);
    ASSERT_EQ(*--b, 0);
    ASSERT_EQ(*--b, 0);
}
TYPED_TEST(DequeFixture, const_it2) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator   = typename TestFixture::const_iterator;
    deque_type x(10);
    x.push_back(3);
    x.push_back(4);
    const deque_type y = x;
    ASSERT_EQ(x,y);
    const_iterator b = y.end();
    ASSERT_EQ(y.size(), 12);
    ASSERT_EQ(*--b, 4);
    ASSERT_EQ(*--b, 3);
    ASSERT_EQ(*--b, 0);
    ASSERT_EQ(*--b, 0);
    ASSERT_EQ(*--b, 0);
}
TYPED_TEST(DequeFixture, it3) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(10, 8);
    x.push_back(3);
    x.push_back(4);
    iterator b = x.end();
    ASSERT_EQ(x.size(), 12);
    ASSERT_EQ(*--b, 4);
    ASSERT_EQ(*b--, 4);
    ASSERT_EQ(*b, 3);
    ASSERT_EQ(*--b, 8);
    ASSERT_EQ(*--b, 8);
    ASSERT_EQ(*--b, 8);
}
TYPED_TEST(DequeFixture, const_it3) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator   = typename TestFixture::const_iterator;
    deque_type x(10, 8);
    x.push_back(3);
    x.push_back(4);
    const deque_type y = x;
    ASSERT_EQ(x,y);
    const_iterator b = y.end();
    ASSERT_EQ(y.size(), 12);
    ASSERT_EQ(*--b, 4);
    ASSERT_EQ(*b--, 4);
    ASSERT_EQ(*b, 3);
    ASSERT_EQ(*--b, 8);
    ASSERT_EQ(*--b, 8);
    ASSERT_EQ(*--b, 8);
}
TYPED_TEST(DequeFixture, pop_front0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(11, 8);
    x.pop_front();
    x.pop_front();

    ASSERT_EQ(x.size(), 9);
}
TYPED_TEST(DequeFixture, begin0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(10);
    x[0] = 10;
    iterator b = x.begin();
    ASSERT_EQ(*b, 10);
}
TYPED_TEST(DequeFixture, pop_front1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(15, 8);
    x.pop_front();
    x.pop_front();
    x.pop_back();
    ASSERT_EQ(x.size(), 12);
    iterator it = x.begin() + 1;
    it = x.insert(it, 5);
    ASSERT_EQ(*it, 5);
    ASSERT_EQ(x[1], 5);
    x.erase(x.begin() + 1);
    iterator b = x.begin();
    iterator e = x.end();
    while(b != e) {
        ASSERT_EQ(*b, 8);
        ++b;
    }
}
TYPED_TEST(DequeFixture, push_front0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(9, 7);
    x.push_front(8);
    x.push_front(8);
    x.push_front(8);
    ASSERT_EQ(x.size(), 12);
    ASSERT_EQ(x[0], 8);
    ASSERT_EQ(x[1], 8);
    ASSERT_EQ(x[2], 8);
    for(int i = 0; i < 9; ++i) {
        x.erase(x.end() - 1);
    }
    ASSERT_EQ(x.size(), 3);
    iterator b = x.begin();
    iterator e = x.end();
    while(b != e) {
        ASSERT_EQ(*b, 8);
        ++b;
    }
}
TYPED_TEST(DequeFixture, pop_back0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(11, 8);
    x.pop_back();
    x.pop_back();
    x.pop_back();
    ASSERT_EQ(x.size(), 8);
    iterator it = x.begin() + 1;
    it = x.insert(it, 4);
    ASSERT_EQ(*it, 4);
    ASSERT_EQ(x[1], 4);
    x.erase(x.begin() + 1);
    iterator b = x.begin();
    iterator e = x.end();
    while(b != e) {
        ASSERT_EQ(*b, 8);
        ++b;
    }
}
TYPED_TEST(DequeFixture, construct0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(5);
    iterator b = x.begin();
    ASSERT_EQ(x.size(), 5);
    for(int i = 0; i < 5; ++i) {
        ASSERT_EQ(x[i], 0);
        ASSERT_EQ(*b, 0);
        ++b;
    }
}

TYPED_TEST(DequeFixture, construct1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(100);
    ASSERT_EQ(x.size(), 100);
    iterator b = x.begin();
    for(int i = 0; i < 100; ++i) {
        ASSERT_EQ(x[i], 0);
        ASSERT_EQ(*b, 0);
        ++b;
    }
    b = x.begin();
    iterator e = x.end();
    while(b != e) {
        ASSERT_EQ(*b, 0);
        ++b;
    }
}

TYPED_TEST(DequeFixture, erase0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(2);
    x.push_back(3);
    x.push_back(4);
    iterator b = x.begin();
    ASSERT_EQ(*(b + 1), 0);
    x.insert(b, 5);
    b = x.begin();
    b = x.erase(b);
    ASSERT_EQ(x.size(), 4);
    ASSERT_EQ(*b, 0);
    ASSERT_EQ(*++b, 0);
    ASSERT_EQ(*++++b, 4);
}

TYPED_TEST(DequeFixture, erase1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(1);
    x.push_back(3);
    x.push_back(4);
    iterator b = x.begin();
    x.insert(b, 5);
    b = x.begin();
    ++b;
    ASSERT_EQ(*b, 0);
    x.erase(b);
    ASSERT_EQ(*b, 5);
    ++b;
    ASSERT_EQ(x.size(), 3);
    ASSERT_EQ(*b, 3);
    ASSERT_EQ(*--b, 5);
}

TYPED_TEST(DequeFixture, test4) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    deque_type y;
    x.push_back(3);
    x.push_back(4);
    y = x;
    ASSERT_EQ(x, y);
    y.push_back(5);
    ASSERT_EQ(x.size(), 2);
    ASSERT_EQ(y.size(), 3);
    ASSERT_NE(x, y);

}

TYPED_TEST(DequeFixture, insert1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    x.push_back(3);
    x.push_back(4);
    x.push_back(5);
    x.push_back(6);
    iterator it = x.begin();
    ++++it;
    x.insert(it, 10);

    ASSERT_EQ(x.size(), 5);
    ASSERT_EQ(x[2], 10);
    ASSERT_EQ(x[3], 5);
    ASSERT_EQ(x[1], 4);

}
TYPED_TEST(DequeFixture, insert3) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(100,5);
    ASSERT_EQ(x.size(), 100);
    iterator it = x.begin() + 50;
    x.insert(it, 100);
    ASSERT_EQ(x[50], 100);
    iterator b = x.begin();
    iterator e = x.end();
    it = x.begin() + 50;
    x.insert(it + 1, 10);

    b = x.begin();
    e = x.end();
    ASSERT_EQ(x.size(), 102);
    ASSERT_EQ(x[50], 100);
    ASSERT_EQ(x[51], 10);

}

TYPED_TEST(DequeFixture, insert4) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(100,5);
    ASSERT_EQ(x.size(), 100);
    iterator it = x.begin() + 50;
    x.insert(it, 100);
    ASSERT_EQ(x[50], 100);
    iterator b = x.begin();
    iterator e = x.end();
    x.insert(it + 1, 10);

    b = x.begin();
    e = x.end();
    ASSERT_EQ(x.size(), 102);
    ASSERT_EQ(x[50], 100);
    ASSERT_EQ(x[51], 10);

}
TYPED_TEST(DequeFixture, it4) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(100,5);
    iterator b = x.begin();
    iterator e = x.end();
    while(b != e) {
        ASSERT_EQ(*b, 5);
        ++b;
    }
}
TYPED_TEST(DequeFixture, const_it4) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator   = typename TestFixture::const_iterator;
    deque_type x(100,5);
    x.push_back(5);
    const deque_type y(x);
    const_iterator b = y.begin();
    const_iterator e = y.end();
    while(b != e) {
        ASSERT_EQ(*b, 5);
        ++b;
    }

}
TYPED_TEST(DequeFixture, test5) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    x.push_back(3);
    x.push_back(4);
    x.push_back(5);
    x.push_back(6);
    iterator it = x.begin();
    ++++it;
    x.insert(it, 10);

    ASSERT_EQ(x.size(), 5);
    ASSERT_EQ(x[2], 10);
    ASSERT_EQ(x[3], 5);
    ASSERT_EQ(x[1], 4);

}
TYPED_TEST(DequeFixture, test6) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    x.push_back(3);
    x.push_front(4);
    iterator it = x.begin();
    it++;
    x.insert(it, 10);

    ASSERT_EQ(x.size(), 3);
    ASSERT_EQ(x[1], 10);
}
TYPED_TEST(DequeFixture, test7) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    x.push_back(3);
    x.push_back(4);
    x.clear();
    ASSERT_EQ(x.size(), 0);
}
TYPED_TEST(DequeFixture, test14) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    x.push_front(3);
    x.push_front(4);
    iterator it = x.begin();
    ASSERT_EQ(*(it++), 4);
    ASSERT_EQ(*(it), 3);
}
TYPED_TEST(DequeFixture, test8) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    x.push_front(3);
    x.insert(x.begin(), 0);

    ASSERT_EQ(x[0], 0);
}
TYPED_TEST(DequeFixture, test9) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    x.push_front(3);
    iterator e = end(x);
    x.insert(e, 5);
    x[0] = 6;

    ASSERT_EQ(x[0], 6);
    ASSERT_EQ(x.size(), 2);
    ASSERT_EQ(*(++(x.begin())), 5);
}
TYPED_TEST(DequeFixture, test10) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    x.push_front(3);
    x[0] = 6;
    x.push_front(5);

    ASSERT_EQ(x[1], 6);
}
TYPED_TEST(DequeFixture, test11) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    x.push_front(3);
    x[0] = 6;

    ASSERT_EQ(*(x.begin()), 6);
}

TYPED_TEST(DequeFixture, it00) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    x.push_back(3);
    iterator b = begin(x);
    iterator e = end(x) - 1;
    EXPECT_TRUE(b == e);
}
TYPED_TEST(DequeFixture, size0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    x.push_front(3);
    x.push_front(5);
    x.clear();
    x.push_front(1);
    x.push_front(2);
    x.push_front(3);
    x.push_back(721);
    x.push_back(8);
    ASSERT_EQ(x.size(), 5);
    iterator b = begin(x);
    iterator e = end(x) - 5;
    EXPECT_TRUE(b == e);
}
TYPED_TEST(DequeFixture, const5) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    deque_type y(x);
    ASSERT_EQ(y, x);
    ASSERT_EQ(y.size(), 0);
    y.push_back(8);
    ASSERT_EQ(y.size(), 1);
    ASSERT_EQ(y[0], 8);
    y.push_front(6);
    ASSERT_EQ(y.size(), 2);
    ASSERT_EQ(y[0], 6);
}

TYPED_TEST(DequeFixture, initializer_construct0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x{0,1,2,3};
    ASSERT_EQ(x.size(), 4);
    iterator b = x.begin();
    iterator e = x.end();
    int i = 0;
    while(b!=e) {
        ASSERT_EQ(*b, i);
        ++b;
        ++i;
    }
}
TYPED_TEST(DequeFixture, initializer_construct1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x{};
    ASSERT_EQ(x.size(), 0);
    iterator b = x.begin();
    iterator e = x.end();
    EXPECT_TRUE(b == e);
}
TYPED_TEST(DequeFixture, erase4) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x{0,1,2,3};
    iterator last = x.end() - 1;
    iterator e = x.erase(last);
    ASSERT_EQ(x.size(), 3);
    ASSERT_EQ(*last, 3);
    EXPECT_TRUE(e == x.end());
}
TYPED_TEST(DequeFixture, COMPARISONS0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x{0,1,2,3};
    deque_type y(4, 9);
    EXPECT_TRUE(y > x);
    EXPECT_TRUE(y >= x);
    EXPECT_TRUE(x < y);
    EXPECT_TRUE(x <= y);
    EXPECT_TRUE(x != y);
    deque_type z;
    for(int i = 0 ; i < x.size(); ++i) {
        z.push_back(i);
    }
    EXPECT_TRUE(z >= x);
    EXPECT_TRUE(z <= x);
    EXPECT_TRUE(z == x);
}

TYPED_TEST(DequeFixture, testPushFrontTillResize) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    const int count = 100;
    for(int i = 0; i < count; ++i)
        x.push_front(i);
    ASSERT_EQ(x.size(), count);
    int j = 0;
    for(int i = 99; i >= 0; --i) {
        ASSERT_EQ(x[j], i);
        ++j;
    }
}


